<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 06.07.15
     * Time: 19:21
     */

    namespace PatternMatcher\Types;


    use PatternMatcher\Errors\ErrorCodes;
    use PatternMatcher\Errors\TypeException;

    class Type {

        /**
         * @param $type
         * @throws TypeException
         * @return IType
         */
        public static function create($type) {
            if (Aliases::exists($type)) {
                $type = Aliases::get($type);
            }

            if (!class_exists($type, true)) {
                throw new TypeException(
                    sprintf('Type `%s` not found', $type),
                    ErrorCodes::TYPE_NOT_FOUND
                );
            }

            $object = new $type();

            if (!($object instanceof IType)) {
                throw new TypeException(sprintf('Class "%s" not implements imterface IType', $type), ErrorCodes::TYPE_CLASS_NOT_ITYPE);
            }

            return $object;
        }

        public static function detect($object) {
            $type = gettype($object);
            if ($type === 'string' && class_exists($object, true)) {
                $type = 'object';
            }

            return $type;
        }

    }