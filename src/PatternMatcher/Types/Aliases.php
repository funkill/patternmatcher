<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 07.07.15
     * Time: 17:50
     */

    namespace PatternMatcher\Types;


    use PatternMatcher\Errors\AliasException;
    use PatternMatcher\Errors\ErrorCodes;

    class Aliases {

        protected static $aliases = [
            'integer' => Integer::class,
            'int' => Integer::class,
            'object' => Object::class,
        ];

        /**
         * Check mixed of alias
         * @param string $type
         * @return bool
         */
        public static function exists($type) {
            return array_key_exists($type, static::$aliases);
        }

        /**
         * Get alias for given type
         * @param mixed $type
         * @throws AliasException
         * @return mixed
         */
        public static function get($type) {
            if (!static::exists($type)) {
                throw new AliasException(
                    sprintf('Can not get type because alias "%s" not exists', $type),
                    ErrorCodes::ALIAS_NOT_EXISTS
                );
            }

            return static::$aliases[$type];
        }

    }