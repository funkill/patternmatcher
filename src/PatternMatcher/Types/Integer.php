<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 06.07.15
     * Time: 19:18
     */

    namespace PatternMatcher\Types;


    use PatternMatcher\Errors\BadValueException;
    use PatternMatcher\Errors\ErrorCodes;

    class Integer implements IType {

        /**
         * @param mixed $match
         * @param array $handlers
         * @throws BadValueException
         * @return mixed
         */
        public function match($match, array $handlers) {
            if (!array_key_exists($match, $handlers)) {
                throw new BadValueException(
                    sprintf('Value `%d` not found', $match),
                    ErrorCodes::VALUE_NOT_FOUND
                );
            }

            return $handlers[$match]($match);
        }

    }