<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 26.06.15
     * Time: 16:28
     */

    namespace PatternMatcher;


    use PatternMatcher\Errors\BadValueException;
    use PatternMatcher\Errors\ErrorCodes;
    use PatternMatcher\Errors\TypeException;
    use PatternMatcher\Types\Type;

    class Matcher {

        protected static $types = [];

        /**
         * @param $match
         * @param array $handlers
         * @throws BadValueException
         * @throws TypeException
         * @return mixed
         */
        public static function match($match, array $handlers) {
            foreach ($handlers as $pattern => $closure) {
                $type = Type::detect($pattern);
                static::$types[$type][$pattern] = $closure;
            }

            $resultType = Type::detect($match);
            if (!array_key_exists($resultType, static::$types)) {
                throw new BadValueException(
                    sprintf('Result type `%s` not found', $resultType),
                    ErrorCodes::TYPE_NOT_FOUND
                );
            }

            /**
             * @hint TypeException not handled because it exception throws only if programmer forgot create type
             */
            $Type = Type::create($resultType);

            return $Type->match($match, static::$types[$resultType]);
        }

    }