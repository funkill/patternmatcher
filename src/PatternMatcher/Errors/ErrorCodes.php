<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 06.07.15
     * Time: 19:30
     */

    namespace PatternMatcher\Errors;


    class ErrorCodes {

        const TYPE_NOT_FOUND = 1;
        const TYPE_CLASS_NOT_ITYPE = 2;
        const VALUE_NOT_FOUND = 3;
        const ALIAS_NOT_EXISTS = 4;

    }