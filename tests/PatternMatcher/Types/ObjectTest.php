<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 07.07.15
     * Time: 18:42
     */

    namespace PatternMatcher\Types;

    use PatternMatcher\Errors\BadValueException;
    use PatternMatcher\Errors\ErrorCodes;

    class ObjectTest extends \PHPUnit_Framework_TestCase {

        /**
         * @var Object
         */
        protected $Object;

        protected $input = [];

        protected $matches;

        public function setUp() {
            $this->Object = new Object();
            $this->input = [
                new Object(),
                new Integer(),
            ];
            $this->matches = [
                Object::class => function ($el) {return true;},
                IType::class => function($el) {return true;},
            ];
        }

        public function testExistInput() {
            $this->assertTrue($this->Object->match($this->input[0], $this->matches));
        }

        public function testInstance() {
            $this->assertTrue($this->Object->match($this->input[1], $this->matches));
        }

        public function testNotExistInput() {
            try{
                $this->Object->match(new static(), $this->matches);
            } catch (BadValueException $E) {
                $this->assertEquals(ErrorCodes::VALUE_NOT_FOUND, $E->getCode());
            }
        }

    }
