<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 06.07.15
     * Time: 19:26
     */

    namespace PatternMatcher\Types;


    use PatternMatcher\Errors\ErrorCodes;
    use PatternMatcher\Errors\TypeException;

    class TestTypeClass {}

    class TypeTest extends \PHPUnit_Framework_TestCase {

        protected $Type;

        public function setUp() {
            $this->Type = null;
        }

        public function testNotExistsType() {
            try {
                $this->Type = Type::create('not_exists_type');
            } catch (TypeException $E) {
                $this->assertEquals(ErrorCodes::TYPE_NOT_FOUND, $E->getCode());
            }
            $this->assertNull($this->Type);
        }

        public function testWrongClass() {
            try {
                $this->Type = Type::create(TestTypeClass::class);
            } catch (TypeException $E) {
                $this->assertEquals(ErrorCodes::TYPE_CLASS_NOT_ITYPE, $E->getCode());
            }
            $this->assertNull($this->Type);
        }

        public function testCreate() {
            $Type = Type::create(Integer::class);
            $this->assertNotNull($Type);
            $this->assertInstanceOf(Integer::class, $Type);
        }

        public function testAlias() {
            $Type = Type::create('int');
            $this->assertNotNull($Type);
            $this->assertInstanceOf(Integer::class, $Type);
        }

        public function testDetectInt() {
            $type = Type::detect(5);
            $this->assertEquals(Integer::class, Aliases::get($type));
        }

        public function testDetectObject() {
            $type = Type::detect(new static());
            $this->assertEquals('object', $type);
        }

        public function testDetectObjectString() {
            $example = [
                Type::class => '1'
            ];
            $types = array_keys($example);
            $type = array_shift($types);
            $resultType = Type::detect($type);
            $this->assertEquals('object', $resultType);
        }

    }

