<?php
    /**
     * Created by PhpStorm.
     * User: funkill
     * Date: 07.07.15
     * Time: 17:52
     */

    namespace PatternMatcher\Types;


    use PatternMatcher\Errors\AliasException;
    use PatternMatcher\Errors\ErrorCodes;

    class AliasesTest extends \PHPUnit_Framework_TestCase {

        protected $type = Integer::class;
        protected $alias = 'integer';


        public function testNotExists() {
            $result = Aliases::exists($this->alias . '_wrong');
            $this->assertFalse($result);
        }

        public function testExist() {
            $result = Aliases::exists($this->alias);
            $this->assertTrue($result);
        }

        public function testGetException() {
            try {
                Aliases::get($this->alias . '_wrong');
            } catch (AliasException $AE) {
                $this->assertEquals(ErrorCodes::ALIAS_NOT_EXISTS, $AE->getCode());
            } catch (\Exception $E) {
                $this->fail('Unknown exception: '. $E->getMessage());
            }
        }

        public function testGetAlias() {
            $alias = null;
            try {
                $alias = Aliases::get($this->alias);
            } catch (\Exception $E) {
                $this->fail(sprintf('Exception `%s` on positive test `getAlias`', $E->getMessage()));
            }

            $this->assertNotNull($alias);
            $this->assertEquals($this->type, $alias);
        }

    }
